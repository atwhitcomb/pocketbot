"use strict";

// ===================================
// Libraries
// ===================================
let chat 	= require("discord.io"),
	path 	= require("path"),
	Fb 		= require("firebase"),
	fs 		= require("fs");
	//gm 		= require("gm").subClass({ imageMagick: true }),
	//steam 	= require("steam-webapi");

// ===================================
// Modules
// ===================================
let TOKEN 		= require("./core/tokens"),
	command 	= require("./core/command"),
	logger 		= require("./core/logger"),
	persona 	= require("./core/personality"),
	userdata 	= require("./core/userdata"),
	status 		= require("./core/presence"),
	helper		= require("./core/helpers"),
	vars 		= require("./core/vars"),
	dio 		= require("./core/dio"),
	messageManager = require("./core/messages"),
	lucilleCmd  = require("./cmds/lucille");

// ===================================
// Initialize Firebase Stuff
// ===================================
let fire = false;
if ( TOKEN.FBKEY2() != false ) {
	Fb.initializeApp(vars.firebasecfg);

	fire = {
		soldiers: 	Fb.database().ref("players"),
		quotes: 	Fb.database().ref("quote")
	};

	// Ready the user data
	userdata = new userdata(fire.soldiers);
	userdata.load(); // ! -- Might be unneeded?

	logger.log("Public Firebase initialized!", logger.MESSAGE_TYPE.OK);
} else {
	logger.log("Public Firebase not initialized.", logger.MESSAGE_TYPE.Warn);
}

// ====================
// Bot Personas
// ====================
let mjPersona = new persona("Pocketbot", "./assets/avatars/mj.png", vars.emojis.ryionbot),
	mastabot = new persona("Pocketbot", "./assets/avatars/mastabot.png", vars.emojis.mastabot),
	bookbot = new persona("Pocketbot", "./assets/avatars/bookbot.png", vars.emojis.bookbot),
	bookbotCowboy = new persona("Last Man Standing", "./assets/avatars/lms.png", vars.emojis.bookbot),
	unitinfo = new persona("Pocketbot", "./assets/avatars/mastabot.png", vars.emojis.pocketbot),
	lucille = new persona("Pocketbot", "./assets/avatars/lucille.png", vars.emojis.lucille);

// Manager and Groups
let globalCmdManager	= new command.CommandManager("d"),
	basicCmdGroup 		= new command.CommandGroup("basic", mastabot, "Basic commands"),
	ryionbotCmdGroup 	= new command.CommandGroup("ryionbot", mjPersona, "Commands brought to you by RyionBot"),
	ryionbot_ecoCmdGroup= new command.CommandGroup("economy", mjPersona, "RyionBot's currency system commands"),
	matchCmdGroup 		= new command.CommandGroup("matchmake", mastabot, "Handles all the matchmaking commands"),
	crownCmdGroup 		= new command.CommandGroup("crown", mastabot, "Handles \"Crown\" minigame commands"),
	quoteCmdGroup 		= new command.CommandGroup("quote", mastabot, "Let\'s you interface with the quoting system"),
	communityCmdGroup 	= new command.CommandGroup("community", mastabot, "All the basic, most-used community chat commands"),
	keyCmdGroup 		= new command.CommandGroup("key", mastabot, "Alpha tester onboarding commands"),
	adminCmdGroup 		= new command.CommandGroup("admin", mastabot, "Admin/Mod only commands"),
	bookbotCmdGroup		= new command.CommandGroup("bookbot", bookbot, "Informational commands"),
	lmsCmdGroup			= new command.CommandGroup("lms", bookbotCowboy, "Commands for Last Man Standing, a chat minigame made by Glyde"),
	infoCmdGroup		= new command.CommandGroup("unitinfo", unitinfo, "Unit/Trait Information command"),
	streamCmdGroup		= new command.CommandGroup("streaming", mastabot, "Twitch stream related commands"),
	lucilleCmdGroup 	= new command.CommandGroup("lucille", lucille, "Lucille gives us the latest tweets."),
	emojiCmd            = new command.CommandGroup("emoji", mastabot, "All the jumbo emotes");

globalCmdManager.addGroup(basicCmdGroup);
globalCmdManager.addGroup(matchCmdGroup);
globalCmdManager.addGroup(crownCmdGroup);
globalCmdManager.addGroup(quoteCmdGroup);
globalCmdManager.addGroup(communityCmdGroup);
globalCmdManager.addGroup(keyCmdGroup);
globalCmdManager.addGroup(adminCmdGroup);
globalCmdManager.addGroup(ryionbotCmdGroup);
globalCmdManager.addGroup(ryionbot_ecoCmdGroup);
globalCmdManager.addGroup(bookbotCmdGroup);
globalCmdManager.addGroup(lmsCmdGroup);
globalCmdManager.addGroup(infoCmdGroup);
globalCmdManager.addGroup(streamCmdGroup);
globalCmdManager.addGroup(lucilleCmdGroup);
globalCmdManager.addGroup(emojiCmd);

// Clear the log file
logger.clearLogFile();

// Parse the cmds dir and load any commands in there
fs.readdir(path.join(__dirname, "cmds"), function(err, files){
	if(err){
		logger.log(err, logger.MESSAGE_TYPE.Error);
		return;
	}

	for(var i = 0; i < files.length; i++){
		let _cmds = require(path.join(__dirname, "cmds", path.parse(files[i]).name)).commands;

		_cmds.forEach(function(element) {
			globalCmdManager.addCommand(element);
		}, this);
	}
});

logger.log(`Debug mode: ${helper.isDebug()}`, logger.MESSAGE_TYPE.Info);
logger.log(`Running on Heroku: ${helper.isHeroku() ? "true" : "false"}`, logger.MESSAGE_TYPE.Info);

// This stays a var. You change it back to let, we fight
var bot = new chat.Client({ token: TOKEN.TOKEN, autorun: true });

//Init the messageManager
var globalMessageManager = new messageManager.MessageManager();
globalMessageManager.AddChannels(bot);

// ===================================
// Bot Events
// ===================================

bot.on("ready", function() {
	logger.log("Bot logged in successfully.", logger.MESSAGE_TYPE.OK);
	//helper.popCommand( cList );

	// Work around to giving Lucille bot/persona info!
	if (helper.isHeroku() ) lucilleCmd.sendData({
		// User data created by bots
		userdata: userdata,
		// Command manager
		commandManager: globalCmdManager,
		// Message manager
		messageManager: globalMessageManager,
		// Bot client object
		bot: bot,
		// ID of channel the message was sent in
		channelID: vars.chan,
		// ID of the server(guild)
		serverID: vars.chan
	},lucille);
});

bot.on("disconnect", function(err, errcode) {
	logger.log(`Disconnected from Discord... Message: "${err}" Error Code: "${errcode}"`, logger.MESSAGE_TYPE.Error);
	bot.connect();
});

bot.on("presence", function(user, userID, state, game, event) {
	let statusData = {
		// Bot client object
		bot: bot,
		// Name of user who sent the message
		user: user,
		// ID of user who sent the message
		userID: userID,
		// Raw message string
		state: state,
		// Name of game being player OR stream title
		game: game,
		// Reference to the Firebase DB's
		db: fire,
		// Raw event
		e: event
	};

	if (fire) status.onChange(statusData, userdata);
});

let cList = [],
	spammer = [],
	personCool = false;

bot.on("message", function(user, userID, channelID, message, event) {
	//console.log(`${user} : ${bot.servers[vars.chan].members[userID].roles}`) // Quick role
	//console.log(`${message}`) // Message check

	//Remove whitespace
	message = helper.collapseWhitespace(message);

	//Split message into args
	let args = helper.getArgs(message);

	//Prepare command_data object
	let command_data = {
		// User data created by bots
		userdata: userdata,
		// Command manager
		commandManager: globalCmdManager,
		// Message manager
		messageManager: globalMessageManager,
		// Bot client object
		bot: bot,
		// Name of user who sent the message
		user: user,
		// ID of user who sent the message
		userID: userID,
		// ID of channel the message was sent in
		channelID: channelID,
		// ID of the server(guild)
		serverID: vars.chan,
		// Raw message string
		message: message,
		// ID of the message sent
		messageID: event.d.id,
		// Attachments Array
		attachments: event.d.attachments,
		// Array of arguments/words in the message
		args: args,
		// Reference to the Firebase DB's
		db: fire,
		// Actually give an event..
		event: event
	};

	globalMessageManager.Push(new messageManager.Message(command_data.messageID, command_data.message, command_data.userID, command_data.channelID, Date.now()), command_data.channelID);

	// Dance Detector
	if ((message.includes("o") || message.includes("0")) && userID === "149541152322879489" ) {
		if ( message.includes("/") || message.includes("\\") || message.includes("<") || message.includes(">") ) {
			//console.log("Dancer Detected");
			logger.log("Dancer Detected", logger.MESSAGE_TYPE.Warn);
			command_data.dance = event.d.id;
		}
	}

	// If from Mastabot, check for timed message otherwise ignore
	if (userID === bot.id) {
		if (message.includes("🕑")) {
			helper.countdownMessage(event.d.id,message,channelID,5,bot);
		} else {
			return false;
		}
	}

    // ===================================
    // SPAM Control
    // ===================================
	let speaker = bot.servers[vars.chan].members[userID];
	if ((speaker && speaker.hasOwnProperty("roles") && speaker.roles.includes(vars.member)) || userID == bot.id ) {
		// return false;
	} else {
		cList.push(userID);
		let c = helper.getCount(cList,userID); // Check how many messages user has posted recently
		//if (channelID != vars.testing) return false;
		if (c===3) {
			if ( spammer.includes(userID) ) {
				// 2nd warning = automute
				helper.muteID({
					data: command_data,
					muteme: userID
				});
			} else {
				// Record warning, auto delete after 5 min.
				spammer.push(userID);
				setTimeout( function() {
					spammer.splice( spammer.indexOf(userID) , 1);
				},1000*60*5);

				// Warning
				let v = [
						`Take it easy on the spam <@${userID}>. :warning:`,
						`<@${userID}> simmer down please. :neutral_face:`,
						`<@${userID}> take a chill pill. :pill:`,
						`Calm down <@${userID}>, no one likes the spam. :unamused:`
					],
					n = Math.floor( Math.random()*4 );
				dio.say(v[n], command_data);
			}
		} else if (c>4) {
			dio.say(`<@${userID}>, you are going to be muted for the next 2 minutes. Please adjust your chat etiquette.`, command_data);

			// Add them to mute role, and remove in 2 minutes
			helper.muteID({
				data: command_data,
				muteme: userID
			});
		}

		setTimeout( function() {
			cList.splice( cList.indexOf(userID) , 1);
		},3000);
	}

	try {
		// Log direct messages to Pocketbot OR all messages in debug mode
		if (channelID === vars.modchan) {
			// Stop spying on us Freakspot.
		} else if (!(channelID in bot.directMessages)) {
			if ( helper.isDebug() ) logger.log(`[#${bot.servers[vars.chan].channels[channelID].name}] ${user}: ${message}`);
		} else {
			logger.log(`[DIRECT MESSAGE] ${user}: ${message}`);
		}

		let cmd_trigger = globalCmdManager.isTrigger(args);

		if (message && cmd_trigger){
			let cmd = globalCmdManager.getCommand(args);
			let cmdGroup = globalCmdManager.getGroup(cmd.groupName);

			command_data.trigger = cmd_trigger;

			// Personality Check
			if (globalCmdManager.activePersona != cmdGroup.personality) {
				if (!personCool) {
					cmdGroup.personality.set(command_data, function() {
						globalCmdManager.call(command_data, cmd, cmdGroup);
					});

					globalCmdManager.activePersona = cmdGroup.personality;

					personCool = true;
					setTimeout( () => {
						personCool = false;
					}, 1000*60*10);
				} else {
					globalCmdManager.call(command_data, cmd, cmdGroup);
				}
			} else {
				globalCmdManager.call(command_data, cmd, cmdGroup);
			}
		} else {
			return;
		}
	} catch(e) {
		bot.sendMessage({
			to: channelID,
			message: `An error occured while looking up or trying to call \`${args[0]}\``
		});
		logger.log(`An error occured while looking up or trying to call \`${args[0]}\``, logger.MESSAGE_TYPE.Error, e);
	}
});

bot.on("guildMemberAdd", function(member) {
	let fromID = member.id,
		data = {bot: bot, db: fire};
	// Stuff to tell new person
	let v = [
		`Welcome to the Pocketwatch chat, <@${fromID}>. Please checkout the <#${vars.rules}> channel for some basic community rules.`,
		`Ahoy <@${fromID}>, welcome! Checkout the <#${vars.rules}> channel for some basic community rules.`,
		`Glad to have you here, <@${fromID}>. Make sure to peek into the <#${vars.rules}> channel to get started.`,
		`What's up <@${fromID}>, welcome to the chat! Make sure to read the <#${vars.rules}> channel to get started.`
	];
	let n = Math.floor( Math.random()*v.length );
	dio.say(v[n], data, vars.chan);

	dio.say("Glad you found the Pocketwatch community, we hope you enjoy your stay. :)\n\n"+

"If you haven't already, please checkout the <#335459145203580938> channel for some basic community rules. If you ever need my help, feel free to type in `!help` in any channel or here in a private message. :thumbs_up:\n\n"+

"**Registration Steps:**\n"+
":bust_in_silhouette: Your user ID is: `" + fromID + "`\n"+
":one: Pick an avatar for Discord, this helps us recognize you better.\n"+
":two: Determine your pin number, which is the first 4 digits of your user ID.\n"+
":three: Type in `!legend` in the <#172429393501749248>. You will need this to convert your number pin to your unit pin.\n"+
":four: Type in `!register` followed by your emoji pin in the <#172429393501749248>. If I were to register, I would type in: `!register :tntsquirrel: :tntskunk: :tntsquirrel: :tntskunk:`. Yours may be different.\n"+
":five: If you did everything correctly, then congratulations on your status as a recruit! :tada:\n"+

"**Sorry! We are no longer allowing further access into the alpha/beta. However, the game is available for pre-order and will launch on Sept. 12th, 2017. Again, pre-ordering does not grant access into the alpha/beta.**",data,fromID);
});

// Key Giveaway Event
fire.soldiers.on("child_changed", (snap) => {
	let user = snap.val();

	// If the user is the winner
	if (user.hasOwnProperty("winner")) {
		// Remove the winner thing
		fire.soldiers.child(user.id).update({
			winner: null,
			vote: null
		});

		// Fire key giving command
		dio.say(`:tada: <@${user.id}> has won the TnT Alpha Key Lotto! Aww yiss. :confetti_ball: Keep watching the stream for key giveaway hype! <http://www.twitch.tv/Pocketwatch>`, {bot: bot}, vars.chan);
	}
});
