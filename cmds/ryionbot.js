var command = require("../core/command").Command,
	helpers = require("../core/helpers"),
	dio     = require("../core/dio");

var cmd_invite = new command("ryionbot", "!invite", "Sends the official invite link of the PWG server", function(data){
	dio.say("***The official PWG Discord server invite link:***\n"+helpers.vars.emojis.discord+" https://discord.gg/pocketwatch", data);
});

var cmd_whois = new command("ryionbot", "!whois", "Displays a user's self written short description", function(data){
    //Check if the a username was given
	if(data.args.length != 2){
		dio.say("A single username is needed for `!whois <username>`",data);
		return;
	}

	var targetID = helpers.getUser(data.args[1]);
	if(targetID === "@everyone" || targetID === "@here") {
		dio.say("Yeah. Nice try...",data);
		return;
	}
    //Check if the user exists
	if(!targetID || !data.bot.users[targetID]){
		dio.say("User '" + data.args[1] + "' does not exist", data);
		return;
	}

    //Check if the user has a description set
	data.userdata.getProp(targetID, "description").then((res) => {
		if(!res){
			dio.say("No user description found for '" + data.args[1] + "'!", data);
			return;
		}

		dio.say(helpers.getNickFromId(targetID, data.bot)+ ": " + res, data);
	});
});

var cmd_iam = new command("ryionbot", "!iam", "Sets a short description for yourself", function(data){
    //Check if the a username was given
	if(data.args.length != 2){
		dio.say("Wrong number of arguments. Syntax is `!iam \"description\"`", data);
		return;
	}
	if(data.args[1].indexOf("@everyone") !== -1 || data.args[1].indexOf("@here") !== -1) {
		dio.say("Yeah. Nice try...", data);
		return;
	}
	data.userdata.setProp({
		user: data.userID,
		prop: {
			name: "description",
			data: data.args[1]
		}
	});

	dio.say(data.user + ": " + data.args[1], data);
});

var cmd_roll = new command("ryionbot", "!roll", "Returns result of the dice roll formula. (Defaults to 1d20. Supports '+'' and '-'. Max 100 dice and 100 sides.)", function(data){

	var args = data.args.slice(1);
	//If no range given, default to 1d20
	if (args.length == 0) {
		args = ["1d20"];
	}

	//Goes through the combined string to parse everything out.
	var gen_roll_dict = function(input, pos) {
		if(!/^(\d*d)?\d+$/i.test(input)) {
			return {error: "Input is not of format [Number]d[Number] or [Number]"};
		}
		var nums = input.split(/d/i);
		var num_dice;
		var num_sides;
		if (nums.length == 1) {
			num_dice = nums[0];
			num_sides = 1;
		} else if (nums.length == 2) {
			num_dice = nums[0] || 1;
			num_sides = nums[1];
		}

		num_dice = parseInt(num_dice, 10);
		num_sides = parseInt(num_sides, 10);

		if (num_dice == 0) {
			return {error: "Number of dice for input is 0"};
		} else if (num_sides == 0) {
			return {error: "Number of sides for input is 0"};
		} else if (num_sides > 100) {
			return {error: "Number of sides is above allowed maximum (max 100)"};
		}

		return {dice: num_dice, sides: num_sides, pos: pos};
	};
	var combined_data = args.join('');
	var rolls = [];
	var plus_split = combined_data.split('+');
	var total_dice = 0;
	for (var i = 0; i < plus_split.length; i++) {
		var component = plus_split[i];
		var minus_split = component.split('-');
		for (var j = 0; j < minus_split.length; j++) {
			var subcomponent = minus_split[j];
			var roll_dict = gen_roll_dict(subcomponent, j == 0);
			if (roll_dict.error) {
				dio.say(roll_dict.error + ": " + subcomponent, data);
				return;
			} else if (roll_dict.sides != 1) {
				total_dice += roll_dict.dice;
				if (total_dice > 100) {
					dio.say("Rolling way too many dice. Shortcircuiting this process.", data);
    				return;
				}
			}
			rolls.push(roll_dict);
		}
	}

	//The rolls!
	var total_sum = 0;
	var results = [];
    for (var i = 0; i < rolls.length; i++) {
    	var roll_dict = rolls[i];
    	var dice_to_roll = roll_dict.dice;
    	var sides_to_roll = roll_dict.sides;
    	var positive = roll_dict.pos;
    	var roll_total = 0;
    	if (sides_to_roll == 1) {
    		roll_total = dice_to_roll;
    	} else {
    		var individual_rolls = [];
    		for (var j = 0; j < dice_to_roll; j++) {
    			var result = Math.floor(Math.random() * sides_to_roll) + 1;
    			roll_total += result;
    			individual_rolls.push(result);
    		}
    		var result_dict = {dice: dice_to_roll, sides: sides_to_roll, total: roll_total, individual_rolls: individual_rolls};
    		results.push(result_dict);
    	}

    	if (positive) {
    		total_sum += roll_total;
    	} else {
    		total_sum -= roll_total;
    	}
    }

    //Time to format the results string.
    var print_out = "";
    for (var i = 0; i < results.length; i++) {
    	var result = results[i];
    	print_out += result.dice + "d" + result.sides + ": **" + result.total + "** (" + result.individual_rolls.join(', ') + ")\n";
    }
    print_out += "Total: " + total_sum;
	dio.say(print_out, data);
});

var cmd_cointoss = new command("ryionbot", "!coin", "Tosses a coin, result is Heads or Tails", function(data){
	dio.say(((Math.round(Math.random() * 300)) % 2 ? "Heads" : "Tails"), data);
});

var cmd_mstack_del = new command("ryionbot", "!purge", "Deletes the last *n* messages", function(data){
	if(data.args.length != 2){
		throw new Error("Wrong number of arguments given to purge command");
	}

	var del_count = parseInt(data.args[1]);

	if(del_count <= 0){
		throw new Error("Number of messages cannot be negative or 0");
	}

	data.messageManager.Delete(del_count + 1, data.channelID, data);

	dio.say("<@" + data.userID + "> nuked the last " + del_count + " messages", data);
});

cmd_mstack_del.permissions = [helpers.vars.ranger, helpers.vars.mod];

var cmd_mstack_find = new command("ryionbot", "!findmessages", "Retrieves a user's messages. This is mostly a debug command", function(data){
	if(data.args.length != 2){
		throw new Error("Wrong number of arguments given to purge command");
	}

	var targetID = helpers.getUser(data.args[1]);
	var messageArray = data.messageManager.GetUserMessages(targetID);

	dio.say(JSON.stringify(messageArray), data);
});

cmd_mstack_find.permissions = [helpers.vars.ranger, helpers.vars.mod];


module.exports.commands = [cmd_invite, cmd_whois, cmd_iam, cmd_roll, cmd_cointoss, cmd_mstack_del, cmd_mstack_find];
