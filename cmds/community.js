/* ----------------------------------------
	This file contains most of the general
	commands (formerly in Mastabot) that the
	public community uses.
 ---------------------------------------- */

let logger  = require("../core/logger"),
	command = require("../core/command").Command,
	dio		= require("../core/dio"),
	x 		= require("../core/vars");

let cmdAlpha = new command("community", "!alpha", "Gives information about joining the alpha", function(data) {
	dio.del(data.messageID, data);
	dio.say(`:pencil: Learn more about the alpha in the <#${x.rules}> channel. :thumbsup:`,data);
});

let cmdControls = new command("community", "!controls", "Display game controls", function(data) {
	dio.say(":video_game: These are the current controls: http://www.toothandtailgame.com/images/big/ctrl.png",data);
});

let cmdBugs = new command("community", "!bugs", "Link to the troubleshooting channel", function(data) {
	dio.say(":beetle: Report bugs here: <#134810918113509376>",data);
});

let cmdWiki = new command("community", "!wiki", "Link to Freakspot's TnT Wiki", function(data) {
	dio.say("Check out the wiki: http://toothandtailwiki.com/",data);
});

let cmdTourney = new command("community", "!tournament", "Link to the Clash of Comrades website", function(data) {
	dio.say("For Tournament and game info, VODs, and other community events, check out http://www.clashofcomrades.com",data);
});

let cmdBalance = new command("community", "!balance", "Links moderators to balance sheet", function(data) {
	dio.del(data.messageID,data);
	dio.say("http://codepen.io/mastastealth/full/5701b12140c6d7f5bccf3b6a43faee08", data, data.userID);
});

cmdBalance.permissions = [x.mod, x.admin];

let cmdKillDancer = new command("community", "!killdancer", "Kills any dancers Hox posts", function(data) {

	if (data.dance != "") {
		logger.log(data.dance, logger.MESSAGE_TYPE.Info);
		dio.del(data.messageID, data);
		dio.del(data.dance, data);
		let weapons = [
			":knife::astonished:",
			":scream::gun:",
			":dagger::astonished:",
			":bomb: :skull:"
		];
		dio.say(weapons[Math.floor(Math.random()*weapons.length)], data);
	}
});

cmdKillDancer.permissions = [x.mod, x.admin];

let cmd8Ball = new command("community", "!8ball", "Asks the 8ball a question", function(data) {

	let x = Math.floor(Math.random()*(20)-1),
		answer = [
			"It is certain", "It is decidedly so", "Without a doubt", "Yes, definitely",
			"As I see it, yes", "Most likely", "Outlook good", "Yes",
			"Signs point to yes", "Reply hazy try again", "Ask again later", "Better not tell you now",
			"Cannot predict now", "Concentrate and ask again", "Don't count on it", "My reply is no",
			"My sources say no", "Outlook not so good", "Very doubtful"
		];

	dio.say(`:8ball: says _"${answer[x]}"_`,data);
});

let cmdLegend = new command("community", "!legend", "Gives the unit/code legend for registering", function(data) {
	dio.del(data.messageID, data);
	dio.say(`<@${data.userID}>, the legend is:
:zero:  ${x.emojis.pigeon}
:one:  ${x.emojis.squirrel}
:two:  ${x.emojis.lizard}
:three:  ${x.emojis.toad}
:four:  ${x.emojis.mole}
:five:  ${x.emojis.falcon}
:six:  ${x.emojis.ferret}
:seven:  ${x.emojis.skunk}
:eight:  ${x.emojis.snake}
:nine:  ${x.emojis.chameleon}
Hover over each emoji to find out how to type them.`,data, x.playground);
});

let cmdRegister = new command("community", "!register", "Registers a user as a recruit", (data) => {
	// Check that user has no roles
	if (data.bot.servers[x.chan].members[data.userID].roles.length != 0) {
		dio.del(data.messageID, data);
		dio.say("You're already a standard user. If you'd like to _lose_ your current role, please see if Masta will ban you.",data);
	} else {
		// Get ID Pin
		let rawpin = data.message.replace("!register","").replace(/\s/g,""),
			realpin = data.userID.slice(0,4),
		// Replace & Compare
			temppin = "";

		for (let i=0, len = realpin.length; i<len; i++) {
			switch (realpin[i]) {
			case "0":
				temppin += `${x.emojis.pigeon}`;
				break;
			case "1":
				temppin += `${x.emojis.squirrel}`;
				break;
			case "2":
				temppin += `${x.emojis.lizard}`;
				break;
			case "3":
				temppin += `${x.emojis.toad}`;
				break;
			case "4":
				temppin += `${x.emojis.mole}`;
				break;
			case "5":
				temppin += `${x.emojis.falcon}`;
				break;
			case "6":
				temppin += `${x.emojis.ferret}`;
				break;
			case "7":
				temppin += `${x.emojis.skunk}`;
				break;
			case "8":
				temppin += `${x.emojis.snake}`;
				break;
			case "9":
				temppin += `${x.emojis.chameleon}`;
				break;
			}
		}

		if (temppin === rawpin) {
			// Add them to Recruit
			data.bot.addToRole({
				serverID: x.chan,
				userID: data.userID,
				roleID: x.noob
			}, (err, res) => {
				if (err) logger.log(`Aww frickin': ${err} | ${res}`,"Error");
				dio.say(":tada: You have been successfully registered!", data);
				dio.say(`:mailbox: <@${data.userID}> is now a Recruit.`, data, x.history);
			});
		} else {
			dio.say("🕑 You seem to have mistyped your code. Use the emoji list to select the units you need, use the `!legend` command if you need to know which unit represents which number, and check the message I first PMed you for your ID number.",data);
		}
	}
});

module.exports.commands = [cmdBalance, cmdTourney, cmdWiki, cmdBugs, cmdControls, cmdAlpha, cmdKillDancer, cmd8Ball, cmdLegend, cmdRegister];

for (let key in x.gifaliases) {
	module.exports.commands.push(new command("emoji", ":"+key+":", "Sends the animated "+x.aliases[key]+" emote", function(data) {
		dio.del(data.messageID, data);
		let gifkey = data.args[0].replace(":","").replace(":","");
		dio.sendImage("./emoji/"+x.gifaliases[gifkey]+".gif",data); // This could be handled better but this gets the job done
		logger.log("Uploaded "+x.gifaliases[gifkey]+".gif", logger.MESSAGE_TYPE.OK);
	}));
}

for (let key in x.aliases) {
	module.exports.commands.push(new command("emoji", ":"+key+":", "Sends the "+x.aliases[key]+" emote!", function(data) {
		dio.del(data.messageID, data);
		let pickey = data.args[0].replace(":","").replace(":",""); // This could be handled better but this gets the job done
		dio.sendImage("./emoji/"+x.aliases[pickey]+".png", data);
		logger.log("Uploaded "+x.aliases[pickey]+".png", logger.MESSAGE_TYPE.OK);
	}));
	module.exports.commands[module.exports.commands.length-1].triggerType = 0;
}

x.gifemotes.forEach(function(emote){
	module.exports.commands.push(new command("emoji", ":"+emote+":", "Sends the animated "+emote+" emote!", function(data) {
		dio.del(data.messageID, data);
		dio.sendImage("./emoji/"+emote+".gif",data);
		logger.log("Uploaded "+emote+".gif", logger.MESSAGE_TYPE.OK);
	}));
});

x.emotes.forEach(function(emote){
	module.exports.commands.push(new command("emoji", ":"+emote+":", "Sends the "+emote+" emote", function(data) {
		dio.del(data.messageID, data);
		dio.sendImage("./emoji/"+emote+".png",data);
		logger.log("Uploaded "+emote+".png", logger.MESSAGE_TYPE.OK);
	}));
});
