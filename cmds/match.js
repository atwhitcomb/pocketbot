/* ----------------------------------------
	This file controls all the stuff related
	to matchmaking within the chat.
 ---------------------------------------- */

let logger  = require("../core/logger"),
	command = require("../core/command").Command,
	x		= require("../core/vars"),
	helpers = require("../core/helpers"),
	dio 	= require("../core/dio");

let cmdReady = new command("matchmake", ["!ready", "!lfg"], "This marks a player as **'Looking for a Game'**", function(data) {
	//regPlayer(fromID,'');
	let from = data.user,
		fromID = data.userID,
		uRoles = data.bot.servers[x.chan].members[fromID].roles,
		isReady = uRoles.includes(x.lfg);

	if(!uRoles.includes(x.member)) {
		let v = [
			`🕑 Sorry ${from}. You'll need the game first`,
			`🕑 ${from} you can't play a game you don't have`,
			`🕑 You need to own the game before you can do that ${from}!`,
			`🕑 I understand you're excited, but you need the game in order to play it ${from}!`
		];
		dio.say(v[Math.floor(Math.random()*v.length)], data);
		return false;
	}

	dio.del( data.messageID, data);

    // Check if player is already ready
	if (!isReady) {
		data.bot.addToRole({
			serverID: x.chan,
			userID: fromID,
			roleID: x.lfg
		}, function(err,resp) {
			if (err) {
				logger.log(err + " / " + resp, logger.MESSAGE_TYPE.Error);
				return false;
			}

			dio.say(`:ok_hand: Nice <@${fromID}>. I'll see if anyone is <@&${x.lfg}>. Get ready to FIGHT! :crossed_swords:  Click to launch TnT: http://www.toothandtailgame.com/play`, data);
		});
	} else {
		let v = [
			`🕑 Yea ${from}, I'm aware you're ready. Chill, I'll let you know when someone else is. :stuck_out_tongue: `,
			`🕑 Hold your ponies ${from}, I know you're ready, let's wait for someone else. :thinking: `,
			`🕑 I know ${from}. Waiting for someone else to play...`,
			`🕑 Yea yea, now wait for someone else who wants to play, ${from}! :stuck_out_tongue: `,
			`🕑 You were already ready ${from}, but thanks for telling us. Again. :stuck_out_tongue_winking_eye: `
		];

		let n = Math.floor( Math.random()*v.length );
		dio.say(v[n], data);
	}
});

let cmdUnready = new command("matchmake", "!unready", "This unmarks a player as **'Looking for a Game'**", function(data) {
	let from = data.user,
		fromID = data.userID;

	dio.del( data.messageID, data);
	data.bot.removeFromRole({
		serverID: x.chan,
		userID: fromID,
		roleID: x.lfg
	}, function(err, resp) {
		if (err) {
			logger.log(resp, logger.MESSAGE_TYPE.Error);
			return false;
		}

		dio.say(`🕑 Okee dokes ${from}. Unmarking you from the list. :+1:`, data);
	});
});

let cmdForceUnready = new command("matchmake", "!forceunready", "Mod command to unready other players if needed", function(data) {

	if (data.args.length != 2) {
		dio.say("Usage is `!unready @user_id`", data);
		return;
	}

	dio.del( data.messageID, data);
	data.bot.removeFromRole({
		serverID: x.chan,
		userID: helpers.getUser(data.args[1]),
		roleID: x.lfg
	}, function(err, resp) {
		if (err) {
			logger.log(`${err} | ${resp}`, logger.MESSAGE_TYPE.Error);
			return false;
		}

		dio.say(`You've been unreadied, <@${helpers.getUser(data.args[1])}>`, data);
	});
});

cmdForceUnready.permissions = [x.mod, x.admin];


let cmdVerify = new command("matchmake", "!verify", "This posts a Steam link which automatically verifies the TnT directory", function(data) {
	dio.say(":white_check_mark: Click here to verify your TnT files: http://toothandtailgame.com/verify", data);
});

cmdUnready.permissions = [x.lfg];

module.exports.commands = [cmdReady, cmdUnready, cmdForceUnready, cmdVerify];
