/* ----------------------------------------
	This file contains all commands that
	require a higher-level of permission
	than usual, i.e. mods/developers
 ---------------------------------------- */

let logger  = require("../core/logger"),
	command = require("../core/command").Command,
	dio		= require("../core/dio"),
	helpers	= require("../core/helpers"),
	x 		= require("../core/vars");

let STRIKE_COUNT = 3;

let cmdSay = new command("admin", "!say", "Allows Masta to speak as Mastabot", function(data) {
	if (data.userID === x.stealth) dio.say(data.message.replace("!say ",""), data, x.chan);
});

let cmdNewBuild = new command("admin", "new build live","Admin trigger to automatically bring up verification link", function(data) {
	let v = [
		":white_check_mark: A new build means it's time to verify! http://toothandtailgame.com/verify",
		":white_check_mark: DO IT. JUST, DO IT: http://toothandtailgame.com/verify",
		":white_check_mark: Don't be a punk, verify that junk: http://toothandtailgame.com/verify",
		":white_check_mark: Verify the game or else you're lame. http://toothandtailgame.com/verify"
	];

	let n = Math.floor( Math.random()*v.length );
	dio.say(v[n], data);
});

cmdNewBuild.permissions = [x.admin];

let cmdCheck = new command("admin","!check","Gets data about the user being checked",function(data) {
	let u = helpers.getUser(data.message),
		user = data.bot.servers[x.chan].members[u];

	if (!user) {
		dio.say(`🕑 Dont recognize member: \`${u}\``, data);
		return false;
	}

	dio.del(data.messageID, data);

	let uname = user.username,
		nick = (user.nick != undefined) ? `${user.nick}` : "<no nickname>";
	let d = new Date(user["joined_at"]),
		join = `${d.getMonth() + 1}/${d.getDate()}/${d.getFullYear()} @ ${d.getHours()}:${d.getMinutes()}`;

	data.db.soldiers.child(u).once("value", function(snap) {
		let memsnap = snap.val(),
			votes = 0,
			antivotes = 0;
		// Log it
		logger.log(JSON.stringify(memsnap), "Info");
		// If users is in Firebase
		if(memsnap) {
			for ( let modvote in memsnap.vote ) {
				if (memsnap.vote[modvote].ticket) {
					votes++;
				} else { antivotes++; }
			}

			let info_embed = new helpers.Embed({
				title: `${uname} - #${user.id}`,
				color: (user.status === "online") ? 0x33ff33 : 0xff3333,
				description: `**Nickname**: ${nick}\n**Joined:** ${join}\n**Key Tickets**: **${votes - antivotes}** :tickets:  ${(memsnap.strikes) ? `\n**Strikes**: **${+memsnap.strikes}** ⚾` : ""} ${(memsnap.verified) ? `\n_Verified by ${helpers.getNickFromId(memsnap.verified, data.bot)}_ :envelope:`: ""}`
			});

			helpers.getAvatarURLFromId(user.id, data.bot, function(err, res){
				info_embed.setThumbnail(res, 64, 64);
				dio.sendEmbed(info_embed, data);
			});
		} else {
			dio.say("🕑 Error checking user in Firebase.", data);
		}
	});
});

cmdCheck.permissions = [x.mod, x.admin];

// works, albeit throwing errors. Only available to people with the pocketranger group. USE WITH CAUTION!!!
let cmdNoobify = new command("admin", "!noobify", "This will remove the member role.", function(data) {
	let k = data.args[1].slice(2, -1);
	if (k != undefined) {
		data.bot.removeFromRole({
			serverID: x.chan,
			userID: k,
			roleID: x.member
		}, function(err,resp) {
			logger.log("Error: " + err, logger.MESSAGE_TYPE.Error);
			logger.log("Response: " + resp, logger.MESSAGE_TYPE.Warn);
		});

		setTimeout(function() {
			data.bot.addToRole({
				serverID: x.chan,
				userID: k,
				roleID: x.noob
			}, function(err, resp) {
				if (err) logger.log(`${err} / ${resp}`, logger.MESSAGE_TYPE.Error);
			});
		}, 500);
	}
});

cmdNoobify.permissions = [x.ranger, x.mod, x.admin];

let cmdStrike = new command("admin", "!strike", "Allows mods to cast a vote to ban someone.", (data) => {
	dio.del(data.messageID, data);
	let stupid = data.bot.servers[x.chan].members[ helpers.getUser(data.args[1]) ];

	// Can't strike mods, admins, or bots
	if (stupid.roles.includes(x.mod) || stupid.roles.includes(x.admin) || stupid.roles.includes(x.combot)) {
		dio.say("Whoa hey, I can't ban them. Talk to a dev.", data);
		return false;
	}

    // Make sure a strike is accompanied with a picture
	dio.say(`🕑 Remember to add screenshot in history of the offense the strike is for, <@${data.userID}>.`, data);

	data.userdata.getProp(stupid.id, "strikes").then( (strikes) => {
		//console.log(stupid.id, strikes, data.attachments);
		if (!strikes) {
			data.userdata.setProp({
				user: stupid.id,
				prop: {
					name: "strikes",
					data: 1
				}
			});
			dio.say(`:baseball: **${stupid.username} has received their first strike from <@${data.userID}>.**`, data, x.history);
		} else if (strikes+1 < STRIKE_COUNT) {
			data.userdata.setProp({
				user: stupid.id,
				prop: {
					name: "strikes",
					data: strikes+1
				}
			});
			dio.say(`:baseball: **${stupid.username} has received a strike from <@${data.userID}> and now has ${ strikes+1 } strikes.**`, data, x.history);
		} else if (strikes+1 >= STRIKE_COUNT) {
			data.userdata.setProp({
				user: stupid.id,
				prop: {
					name: "strikes",
					data: strikes+1
				}
			});

			// Goodness gracious this is scary xD
			data.bot.ban({
				serverID: x.chan,
				userID: stupid.id
			}, (err)=> {
				if (err) {
					logger.log(`User ban failed. ${err}`,"Error");
					return false;
				} else {
					dio.say(`:baseball: **${stupid.username} has received final strike from <@${data.userID}> and is now BANNED**.`, data, x.history);
				}
			});
		}

		// For anything besides a ban
		if (strikes+1 != STRIKE_COUNT) {
			data.userdata.getProp(stupid.id, "blacklist").then( (listed) => {
				if (!listed) {
					// Add to blacklist
					data.userdata.setProp({
						user: stupid.id,
						prop: {
							name: "blacklist",
							data: 1
						}
					});
				} else {
					// Add more to blacklist
					data.userdata.setProp({
						user: stupid.id,
						prop: {
							name: "blacklist",
							data: listed+1
						}
					});
				}
			});
		}
	});
});

cmdStrike.permissions = [x.mod, x.admin];

let cmdMute = new command("admin", "!mute", "Mutes a user for X minutes", function(data){
	if(data.args.length != 3){
		dio.say("🕑 Wrong number of arguments. Syntax is `!mute user_id time_in_minutes`", data);
		return;
	}

	let targetID = helpers.getUser(data.args[1]),
		time = parseInt(data.args[2]);

	if(!targetID){
		dio.say("🕑 The provided ID is somehow bad", data);
		return;
	}

	if(targetID == data.userID){
		dio.say("This command is for muting others. For muting yourself, I can recommend ductape", data);
		return;
	}

	var targetRoles = helpers.getUserRoles(data.bot, targetID, data.serverID);

	if(targetRoles.indexOf(helpers.vars.admin) != -1){
		dio.say("Developers cannot be muted. No, not even Nguyen or Masta", data);
		return;
	}

	if(targetRoles.indexOf(helpers.vars.muted) != -1){
		dio.say(`<@${targetID}> is already muted`, data);
		return;
	}

	if(Number.isNaN(time)){
		dio.say(`${time} is not a number`, data);
		return;
	}

	if(time < 1 || time > helpers.vars.consts.MAX_TIMEOUT){
		dio.say(`🕑 Invalid time period: must be at least 1 minute and less that ${helpers.vars.consts.MAX_TIMEOUT} minutes`, data);
		return;
	}

	helpers.muteID({
		uid: targetID,
		auth: data.userID,
		data: data,
		time: time
	});
});

cmdMute.permissions = [x.mod, x.admin];

let cmdConfirm = new command("admin", "!confirm", "Marks a user as having signed up", function(data) {
	if(data.args.length != 3){
		dio.say("🕑 Wrong number of arguments. Syntax is `!confirm <@user> [win|mac|nix|-]`", data);
		return;
	}

	let targetID = helpers.getUser(data.args[1]),
		os = data.args[2];

	if (os != "win" && os != "mac" && os != "nix" && os!= "-") {
		dio.say("🕑 Platform argument must either: `win`, `mac`, `nix` or `-`", data);
		return;
	}

	if (!targetID){
		dio.say("🕑 The provided user is somehow bad", data);
		return;
	}

	if(targetID == data.userID){
		dio.say("I'm sorry you feel the need to be validated, but even I can't do that for you.", data);
		return;
	}

	if (os!="-") data.bot.addToRole({
		serverID: x.chan,
		userID: targetID,
		roleID: x[os]
	}, function(err, resp) {
		if (err) logger.log(`${err} / ${resp}`, logger.MESSAGE_TYPE.Error);

		dio.say(`:ballot_box_with_check: <@${targetID}> has been verified as signed up.`, data, x.history);

		data.userdata.setProp({
			user: targetID,
			prop: {
				name: "verified",
				data: data.userID
			}
		});

		data.userdata.setProp({
			user: targetID,
			prop: {
				name: "username",
				data: helpers.getNickFromId(data.userID, data.bot)
			}
		});
	});
});

cmdConfirm.permissions = [x.mod, x.admin];

let cmdRed = new command("admin", ["!codered","!gg","!defcon1"], "Silences community incase of emergencies", function(data) {
	data.bot.editChannelPermissions({
		roleID: x.chan,
		channelID: x.chan,
		deny: [11]
	}, (err, resp) => {
		if (err) {
			logger.log(`${err} | ${resp}`, "Error");
		} else {
			let v = [
				":rotating_light: **HOLY CRAP. Emergency mode activated.** All testers (and below) have been muted.",
				":rotating_light: **OH SNAP. Emergency mode activated.** All testers (and below) have been muted.",
				":rotating_light: **AY MI MADRE. Emergency mode activated.** All testers (and below) have been muted.",
				":rotating_light: **LORD HAVE MERCY. Emergency mode activated.** All testers (and below) have been muted."
			];

			let n = Math.floor( Math.random()*v.length );
			dio.say(v[n], data, x.chan);
		}
	});
});

cmdRed.permissions = [x.mod, x.admin];

let cmdGreen = new command("admin", ["!codegreen","!ez","!defcon5"], "Silences community incase of emergencies", function(data) {
	data.bot.editChannelPermissions({
		roleID: x.chan,
		channelID: x.chan,
		allow: [11]
	}, (err, resp) => {
		if (err) {
			logger.log(`${err} | ${resp}`, "Error");
		} else {
			let v = [
				":balloon: **Emergency mode disactivated.** You may now return to your regularly scheduled programs.",
				":sunglasses: **Emergency mode disactivated.** Be cool, fam.",
				":logo_tnt: **Emergency mode disactivated.** Now go play some TnT or somethin'...",
				":sweat_smile: **Emergency mode disactivated.** Phew! Crisis averted."
			];

			let n = Math.floor( Math.random()*v.length );
			dio.say(v[n], data, x.chan);
		}
	});
});

cmdGreen.permissions = [x.mod, x.admin];

module.exports.commands = [cmdSay, cmdNewBuild, cmdCheck, cmdNoobify, cmdStrike, cmdMute, cmdRed, cmdGreen, cmdConfirm];
