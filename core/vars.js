let fs = require("fs"),
	path = require("path"),
	logger = require("./logger"),
	TOKEN = require("./tokens");

let emotes = function() {
	let _emotes = [],
		_gifemotes = [];
	fs.readdir(path.join(__dirname, "..", "emoji"), function(err, files){
		if(err) return logger.log(err, "Error");
		for(var i = 0; i < files.length; i++){
			if(path.parse(files[i]).ext === ".gif") {
				_gifemotes.push(path.parse(files[i]).name);
			} else {
				_emotes.push(path.parse(files[i]).name);
			}
		}
	});
	return {
		emotes: _emotes,
		gifs: _gifemotes
	};
};

module.exports = {
	chan: TOKEN.SERVERID(), // PWG Server/Main Channel ID
	// Users -----------------------
	stealth: "98419293514919936",
	schatz: "99245922310959104",
	nguyen: "99233194800328704",
	dex: "146375850705551360",
	adam: "115912365966360576",
	nooneImportant: "145425008590716929",
	// Roles -----------------------
	noob: "195983997413752832",
	member: "99502270579740672",
	king: "213077157038129154", // Crown
	lfg: "238808560106995712", // Looking for game
	mod: "99565011696910336",
	admin: "103552189221326848", //Developer
	ranger: "245142907097579520",
	combot: "232847471963930624", // Community bot
	muted: "268150552699863042",
	win: "303514597028003840",
	mac: "177972198055608320",
	nix: "210436394219208704",
	// Channels --------------------
	memchan: "245596925531783168",
	rules: "335459145203580938",
	history: "196362695367196672",
	playground: "172429393501749248",
	modchan: "180446374704316417",
	wikichan: "235495724542853120",
	techtalk: "200666247694778378",
	trouble: "134810918113509376",
	testing: "246281054598594560",
	emotes: emotes().emotes,
	gifemotes: emotes().gifs,
	gifaliases: {
		patch17: "schatzmeteor"
	},
	aliases: {
		yourmother: "yomama"
	},
	firebasecfg: {
		apiKey:             TOKEN.FBKEY2(),
		authDomain: 		"pocketbot-40684.firebaseapp.com",
		databaseURL: 		"https://pocketbot-40684.firebaseio.com",
		storageBucket: 		"pocketbot-40684.appspot.com",
		messagingSenderId: 	"969731605928"
	},
	//Custom emojis
	emojis: {
		//Devs
		schatz: "<:schatz:230393920842891264>",
		nguyen: "<:nguyen:230394513560961024> ",
		masta: "<:masta:230396612797661185>",
		dexter: "<:dexter:230393518386970626>",
		adam: "<:adam:230394845108240384>",
		austin: "<:austin:230397896699281418>",
		//Bots
		ryionbot: "<:ryionbot:247030935021682688>",
		bookbot: "<:bookbot:247030934287810563>",
		mastabot: "<:mastabot:247030934933733376>",
		lucille: "<:lucille:247030934929539073>",
		// Units
		warrent1: "<:warrent1:253571862926196736>",
		warrent2: "<:warrent2:253571862733258754>",
		warrent3: "<:warrent3:253571863026991124>",
		squirrel: "<:tntsquirrel:253727216251174915>",
		toad: "<:tnttoad:253732160706445313>",
		lizard: "<:tntlizard:253727216456695808>",
		mole: "<:tntmole:253728669808197643>",
		pigeon: "<:tntpigeon:253732160878411776>",
		falcon: "<:tnthawk:253728670299062272>",
		ferret: "<:tntferret:253727216398106625>",
		skunk: "<:tntskunk:253730191514402816>",
		snake: "<:tntsnake:253732160916291594>",
		chameleon: "<:tntchameleon:253730191556214784>",
		wolf: "<:tntwolf:230095674283261954>",
		fox: "<:tntfox:253728670009524226>",
		badger: "<:tntbadger:230094635903483904>",
		boar: "<:tntboar:230096277541486592>",
		owl: "<:tntowl:230095674148913164>",
		turret: "<:tntMG:253571159264591873>",
		mine: "<:tntmine:253570337340522496>",
		balloon: "<:tntballoon:253570336967229450>",
		artillery: "<:tntarty:253571159252140032>",
		barbedwire: "<:tntbarbedwire:259083677571481611>",
		//Other
		wip: "<:wip:247433292587073536>",
		discord: "<:logo_discord:266353098887397386>"
	},
	// DON'T DELETE These
	// ==================
	//Other constants
	firebasekeycfg: {
		projectId: "ltf-alpha-keys",
		clientEmail: TOKEN.FBKEYEMAIL(),
		privateKey: TOKEN.FBPKEY()
	},
	consts: {
		MAX_TIMEOUT: 10080 //Max mute period, 1 week for now
	}
};
