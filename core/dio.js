var logger = require("./logger"),
	path = require("path");

/* ----------------------------------------
	These are simplified versions of the
	larger discord.io functions, just so
	we have to write less crap.
 ---------------------------------------- */
var exports = module.exports = {};

exports.say = function(msg,data,chan=false) {
	let c = (chan) ? chan : data.channelID;

	for(let i=0; i < Math.ceil(msg.length / 2000); i++) {
		let newmsg = msg.substring( i*2000, i+1999 );
		data.bot.sendMessage({
			to:c,
			message: newmsg
		});
	}
};

exports.sendEmbed = function(embedObj, data, chan=false){
	let c = (chan) ? chan : data.channelID;
	data.bot.sendMessage({
		to: c,
		embed: embedObj
	}, function(err){
		if(err){
			logger.log(err, logger.MESSAGE_TYPE.Error);
		}
	});
};

exports.del = function(msg,data,chan=false,t=100) {
	let c = (chan) ? chan : data.channelID;
	setTimeout( function() {
		data.bot.deleteMessage({
			channelID: c,
			messageID: msg
		});
	}, t);
};

exports.edit = function(msgID,bot,text,chan=false) {
	let c = (chan) ? chan : bot.channelID;
	bot.editMessage({
		channelID: c,
		messageID: msgID,
		message: text
	});
};

exports.sendImage = function(file,data,msg=false,filename=false,chan=false) {
	let c = (chan) ? chan : data.channelID,
		fname = (filename) ? filename : path.basename(file),
		message = (msg) ? msg : "`@"+data.user+":`";
	data.bot.uploadFile({
		to: c,
		file: file,
		filename: fname,
		message: message
	});
};
